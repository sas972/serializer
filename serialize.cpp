#include <iostream>
using namespace std;


enum ArchiveActionType {in, out};

struct Archive {
	istream& in;
	ostream& out;
	ArchiveActionType actiontype;
	Archive(istream& inp,ostream& outp) : in(inp), out(outp), actiontype(ArchiveActionType::in){};
};

template<typename T>
void operator<<(Archive &a, T &t){
	a.actiontype = out;
	t.serialize(a);
}

template<typename T>
void operator>>(Archive &a, T &t){
	a.actiontype = in;
	t.serialize(a);
}

template<class T>
void operator&(const Archive &a, T &t) {
	if(in == a.actiontype ) a.in >>  t;
	else a.out << t << endl;
}


struct Kuty {
	int a;
	double d;
	string str;

	void serialize(Archive &a) {
		a & str;
		a & d;
		a & this->a;
	}
};


int main(int argc, char **argv) {
	cout << "Hello world" << endl;
	Archive a(cin,cout);
	Kuty k;
	k.str = "Kuty";
	k.a = 42;
	k.d = 21.0;

	a << k;
	cout << endl;
	a >> k;


	cout << endl;
	a << k;

//	cout << k.str;

	return 0;
}
